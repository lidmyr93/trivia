import { settings } from "../../src/game_settings/settings";

/* eslint-disable no-undef */
before(function() {
  cy.clearLocalStorage();
});

function addPoint(selector) {
  cy.get('input[name="answer0"]').click();
  cy.get('input[type="submit"]').click();

  return cy.get(`[data-testid=${selector}]`);
}

describe("Adding players, removing players", function() {
  it("Player have been added", () => {
    cy.visit("/");
    cy.get('input[name="playerName"]')
      .type("player1")
      .should("have.value", "player1");
    cy.get('input[name="addPlayer"]').click();

    cy.get('input[name="playerName"]')
      .clear()
      .type("player2")
      .should("have.value", "player2");
    cy.get('input[name="addPlayer"]').click();

    cy.get('input[name="playerName"]')
      .clear()
      .type("player3")
      .should("have.value", "player3");
    cy.get('input[name="addPlayer"]').click();

    cy.get(".row")
      .children()
      .contains("player1");

    cy.get(".row")
      .children()
      .contains("player2");
  });
  it("removing Players", () => {
    cy.get(".row").should("contain", "player3");
    cy.get('button[value="player3"]').click();
    cy.get(".row").should("not.contain", "player3");
  });
});

describe("Starting the game", () => {
  it("Should be able to press start game", () => {
    cy.get(".button").click();
    cy.get('input[name="playerName"]').should("not.exist");
    cy.get('input[name="addPlayer"]').should("not.exist");
  });
});

describe("Should have correct number of point fields", () => {
  it("Should have correct number of point fields", () => {
    cy.get(`[data-testid="wrapper-player1"]`)
      .children()
      .should("have.length", settings.rightsToWin.length);
    cy.get(`[data-testid="wrapper-player2"]`)
      .children()
      .should("have.length", settings.rightsToWin.length);
  });
});
describe("Players guessing questions", () => {
  it("players answering right question", () => {
    addPoint("player1").should("have.length", 1);
    addPoint("player2").should("have.length", 1);
  });

  it("Players answering wrong question", () => {
    cy.get('input[name="answer1"]').click();

    cy.get('input[type="submit"]').click();

    //
    cy.get(`[data-testid="player2"]`).should("have.length", 1);
  });
});

describe("Player winning the game and resetting the game", () => {
  it("One player winning the game", () => {
    addPoint("player1");
    addPoint("player2");
    addPoint("player1");
    addPoint("player2");
    addPoint("player1");
    addPoint("player2");
    addPoint("player1");

    cy.get('[data-testid="winningPlayer"').should("contain", "player2");
    cy.get('[data-testid="reset"').click();
  });

  it("Game should have been reseted", () => {
    cy.get(".row")
      .children()
      .contains("player1");

    cy.get(".row")
      .children()
      .contains("player2");

    cy.get('input[name="addPlayer"]').should("have.length", 1);
    cy.get('input[name="playerName"]').should("have.length", 1);
    cy.get('[data-testid="player1"]').should("not.exist");
    cy.get('[data-testid="player2"]').should("not.exist");
  });
});
