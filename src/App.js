import React, { useState, useEffect } from "react";

import { getQuestions } from "./api/api";
import { settings } from "./game_settings/settings";
import styled from "styled-components";
import QuestionContainer from "./components/questionContainer/QuestionContainer";

const AppContainer = styled.div`
  @media (min-width: 768px) {
    width: 100vw;
    height: 100vh;
    margin: 0 auto;
    display: flex;
    flex-direction: column;
    align-items: center;
    background: rgb(15, 48, 212);
    background: radial-gradient(
      circle,
      rgba(15, 48, 212, 1) 0%,
      rgba(94, 75, 75, 1) 100%
    );
  }
  width: 80vw;
`;
function App() {
  const getData = async query => {
    await getQuestions(query).then(q => setData(q));
  };
  const [data, setData] = useState(false);
  useEffect(() => {
    getData();
  });
  return (
    <AppContainer>
      <h1>{settings.title}</h1>
      {data ? (
        <QuestionContainer data={data} newSetOfQuestions={getData} />
      ) : (
        <h1>Loading game...</h1>
      )}
      {/* {data && <QuestionContainer data={data} newSetOfQuestions={getData} />} */}
    </AppContainer>
  );
}

export default App;
