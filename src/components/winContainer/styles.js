import styled from "styled-components";

export const WinWrapper = styled.div`
  position: absolute;
  width: 50%;
  height: 80%;
  left: 25%;
  top: 10%;
  background: orange;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: rgb(15, 48, 212);
  background: radial-gradient(
    circle,
    rgba(15, 48, 212, 1) 0%,
    rgba(94, 75, 75, 1) 100%
  );
  border-radius: 100%;
  box-shadow: 6px 10px 50px 17px rgba(0, 0, 0, 0.61);
  p {
    font-size: 2em;
    font-weight: bolder;
    font-family: "Franklin Gothic Medium", "Arial Narrow", Arial, sans-serif;
  }

  button {
    background-color: transparent;
    border: 2px solid white;
    border-radius: 0.6em;
    color: red;
    cursor: pointer;
    display: flex;
    align-self: center;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1;
    margin: 20px;
    padding: 1.2em 2.8em;
    text-decoration: none;
    text-align: center;
    text-transform: uppercase;
    font-family: "Montserrat", sans-serif;
    font-weight: 700;

    &:hover,
    &:focus {
      color: #fff;
      outline: 0;
    }

    border-color: blue;
    color: #fff;
    box-shadow: 0 0 40px 40px blue inset, 0 0 0 0 blue;
    transition: all 150ms ease-in-out;

    &:hover {
      box-shadow: 0 0 10px 0 blue inset, 0 0 10px 4px blue;
      color: blue;
    }
  }
`;
