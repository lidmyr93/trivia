import React from "react";
import { WinWrapper } from "./styles";
export default function WinContainer(props) {
  const { winningPlayer, handleReset } = props;

  const handleClick = e => {
    handleReset();
  };
  return (
    <WinWrapper>
      <p data-testid="winningPlayer">
        Du vann grattis {winningPlayer.playerName}
      </p>
      <button data-testid="reset" onClick={e => handleClick()}>
        Play again
      </button>
    </WinWrapper>
  );
}
