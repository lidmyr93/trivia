import React from "react";
import { settings } from "../../game_settings/settings";
import { PointRow } from "./styles";

export default function PlayerPointTable(props) {
  const { playerScore, player } = props;

  return (
    <PointRow data-testid={`wrapper-${player}`}>
      {settings.rightsToWin.map((wrapper, i) => (
        <span key={i} data-testid={playerScore[i] ? player : null}>
          {playerScore[i] ? "X" : null}
        </span>
      ))}
    </PointRow>
  );
}
