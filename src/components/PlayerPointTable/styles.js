import styled from "styled-components";

export const PointRow = styled.div`
  border: 1px solid black;
  background: white;
  width: 500px;
  height: 20px;
  display: flex;

  span {
    width: calc(500px / 5);
    height: 20px;
    border: 1px solid black;
    text-align: center;
    font-weight: bolder;
  }
  .point {
    margin: 0;
    text-align: center;
  }
`;
