import styled from "styled-components";

export const Container = styled.div`
  width: 90%;
  height: 90%;
  border: 1px solid black;
  background: rgba(255, 255, 255, 0.5);
`;
