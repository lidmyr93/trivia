import React from "react";
import QuestionHeader from "../QuestionHeader/QuestionHeader";
import Answers from "../Answers/Answers";
import PlayerTable from "../playerTable/PlayerTable";
import WinContainer from "../winContainer/WinContainer";
import { settings } from "../../game_settings/settings";
import { Container } from "./styles";

export default class QuestionContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      selectedQuestion: null,
      players: null,
      gameState: false,
      currentPlayer: null,
      winningPlayer: null
    };
  }

  async componentDidMount() {
    try {
      await this.setState({ data: this.props.data });
      this.handleNextQuestion();
    } catch {
      return;
    }
  }

  handleNextQuestion = () => {
    const question = this.state.data.shift();
    this.setState({ selectedQuestion: question });
  };
  handleAddPlayers = addedPlayer => {
    this.state.players === null
      ? this.setState({ players: [addedPlayer] })
      : this.setState({ players: [...this.state.players, addedPlayer] });
  };

  handleRemovePlayers = removedPlayer => {
    const { players } = this.state;

    this.setState({
      players: players.filter(player => player.playerName !== removedPlayer)
    });
  };
  handleNextPlayer = () => {
    const { players } = this.state;
    const nextPlayer = players.shift();
    this.setState({ currentPlayer: nextPlayer });
    players.push(nextPlayer);
    return nextPlayer;
  };
  handleGameStart = () => {
    this.setState({ gameState: true });
    this.handleNextPlayer();
  };
  handleRound = roundInfo => {
    this.handlePoints(roundInfo);
    this.handleNextPlayer();
  };

  handlePoints = roundInfo => {
    const { correct } = roundInfo;

    /* let tempArray = this.state.players;
    let playerToUpdate = this.state.players.pop(); */
    /* playerToUpdate.score.push(correct); */
    /* this.handleWin(playerToUpdate); */
    /* tempArray.push(playerToUpdate); */
    /* this.setState({ players: tempArray }); */
    if (correct) {
      this.state.currentPlayer.score.push(correct);
    }
    if (this.state.currentPlayer.score.length === settings.rightsToWin.length) {
      this.handleWin(this.state.currentPlayer);
    }
    return;
  };

  handleWin = winningPlayer => {
    this.setState({ winningPlayer: winningPlayer });
  };
  handleReset = () => {
    this.setState({
      gameState: false,
      winningPlayer: null
    });
    this.state.players.map(player => (player.score = []));
  };

  render() {
    const {
      players,
      gameState,
      selectedQuestion,
      currentPlayer,
      winningPlayer
    } = this.state;

    return (
      <Container>
        <PlayerTable
          players={players}
          handleAddPlayers={this.handleAddPlayers}
          handleRemovePlayers={this.handleRemovePlayers}
          handleGameStart={this.handleGameStart}
          gameState={gameState}
        />
        {winningPlayer && (
          <WinContainer
            winningPlayer={winningPlayer}
            handleReset={this.handleReset}
          />
        )}
        {gameState && selectedQuestion && (
          <QuestionHeader
            category={selectedQuestion.category}
            question={selectedQuestion.question}
          />
        )}

        {gameState && selectedQuestion && currentPlayer && (
          <Answers
            answers={selectedQuestion}
            getNextQuestion={this.handleNextQuestion}
            handleRound={this.handleRound}
            currentPlayer={this.currentPlayer}
          />
        )}
        {/* <History /> */}
      </Container>
    );
  }
}
