import React from "react";
import { Header } from "./styles";
export default function QuestionHeader(props) {
  const { category, question } = props;

  return (
    <Header>
      <h2 data-testid="category">{decodeURIComponent(category)}</h2>
      <h1 data-testid="question">{decodeURIComponent(question)}</h1>
    </Header>
  );
}
