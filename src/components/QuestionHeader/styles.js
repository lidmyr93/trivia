import styled from "styled-components";

export const Header = styled.div`
  width: 80%;
  height: auto;
  margin: 0 auto;
  text-align: center;
`;
