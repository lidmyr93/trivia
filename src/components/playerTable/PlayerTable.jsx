import React from "react";
import Player from "../Player/Player";
import { PlayerContainer, StyledForm, FormContainer } from "./styles";

export default class PlayerTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      addedPlayer: null,
      index: 0
    };
  }
  handleSubmit = e => {
    const { handleAddPlayers } = this.props;
    const { addedPlayer, index } = this.state;
    e.preventDefault();
    handleAddPlayers({ ...addedPlayer, index: index, score: [] });
    this.setState({ index: index + 1 });
  };
  handleChange = e => {
    this.setState({ addedPlayer: { playerName: e.target.value } });
  };
  handleRemove = e => {
    const { handleRemovePlayers } = this.props;
    handleRemovePlayers(e.target.value);
  };
  handleStartClick = () => {
    const { handleGameStart } = this.props;
    this.setState({ addedPlayer: null });
    this.setState({ index: 0 });
    handleGameStart();
  };
  render() {
    const { players, gameState } = this.props;
    const { handleRemove, handleSubmit, handleChange, handleStartClick } = this;
    return (
      <PlayerContainer>
        <div className="header">Players</div>

        {players &&
          players.map((p, i) => (
            <Player
              key={i}
              playerName={p.playerName}
              gameState={gameState}
              handleRemove={handleRemove}
              playerScore={p.score}
            />
          ))}

        <FormContainer>
          {!gameState && (
            <StyledForm
              onSubmit={e => {
                handleSubmit(e);
              }}
            >
              <div>
                <input
                  name="playerName"
                  type="text"
                  placeholder="Player name..."
                  onChange={e => handleChange(e)}
                />
              </div>
              <input name="addPlayer" type="submit" value="Add Player" />
            </StyledForm>
          )}
          {!gameState && (
            <button onClick={() => handleStartClick()} className="button">
              Start
            </button>
          )}
        </FormContainer>
      </PlayerContainer>
    );
  }
}
