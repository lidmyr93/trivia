import styled from "styled-components";

export const PlayerContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  min-height: 30%;
  min-width: 80%;
  .header {
    font-size: 2em;
    font-weight: 400;
    padding: 20px 0;
  }
`;
export const FormContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  justify-content: space-between;
  height: 10vh;
  width: 100%;
  button {
    background-color: transparent;
    border: 2px solid red;
    border-radius: 0.6em;
    color: red;
    cursor: pointer;
    display: flex;
    align-self: center;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1;
    margin: 20px;
    padding: 1.2em 2.8em;
    text-decoration: none;
    text-align: center;
    text-transform: uppercase;
    font-family: "Montserrat", sans-serif;
    font-weight: 700;

    &:hover,
    &:focus {
      color: #fff;
      outline: 0;
    }

    border-color: green;
    color: #fff;
    box-shadow: 0 0 40px 40px green inset, 0 0 0 0 green;
    transition: all 150ms ease-in-out;

    &:hover {
      box-shadow: 0 0 10px 0 green inset, 0 0 10px 4px green;
      color: green;
    }
  }
`;
export const StyledForm = styled.form`
  background: transparent;
  width: 50%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  input[type="text"] {
    font-family: inherit;
    width: 100%;
    border: 0;
    outline: 0;
    font-size: 1.3rem;
    color: black;
    padding: 7px 0 7px 7px;
    background: transparent;
    border-bottom: 1px solid black;
  }
  input[type="text"]::-webkit-input-placeholder {
    color: black;
  }
  input[type="submit"] {
    background-color: transparent;
    border: 2px solid red;
    border-radius: 0.6em;
    color: $red;
    cursor: pointer;
    display: flex;
    align-self: center;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1;
    margin: 20px;
    padding: 1.2em 2.8em;
    text-decoration: none;
    text-align: center;
    text-transform: uppercase;
    font-family: "Montserrat", sans-serif;
    font-weight: 700;

    &:hover,
    &:focus {
      color: #fff;
      outline: 0;
    }

    border-color: blue;
    color: #fff;
    box-shadow: 0 0 40px 40px blue inset, 0 0 0 0 blue;
    transition: all 150ms ease-in-out;

    &:hover {
      box-shadow: 0 0 10px 0 blue inset, 0 0 10px 4px blue;
      color: blue;
    }
  }
`;
