import styled from "styled-components";

export const AnswersForm = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  input[type="submit"] {
    background-color: transparent;
    border: 2px solid red;
    border-radius: 0.6em;
    color: red;
    cursor: pointer;
    display: flex;
    align-self: center;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1;
    margin: 20px;
    padding: 1.2em 2.8em;
    text-decoration: none;
    text-align: center;
    text-transform: uppercase;
    font-family: "Montserrat", sans-serif;
    font-weight: 700;

    &:hover,
    &:focus {
      color: #fff;
      outline: 0;
    }

    border-color: green;
    color: #fff;
    box-shadow: 0 0 40px 40px green inset, 0 0 0 0 green;
    transition: all 150ms ease-in-out;

    &:hover {
      box-shadow: 0 0 10px 0 green inset, 0 0 10px 4px green;
      color: green;
    }
  }
`;
export const Card = styled.div`
  width: 50%;
  height: 50%;
`;
export const StyledAnswer = styled.div`
  display: flex;
  justify-content: space-between;
  max-width: 50%;
  margin: 0 auto;
  margin-bottom: 10px;
  input[type="checkbox"] + label {
    display: block;
    width: 50%;
  }
  input[type="checkbox"] {
    width: 30px;
    height: 30px;
  }
  label {
    font-size: 2em;
  }
`;
