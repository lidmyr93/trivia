import React from "react";

import { AnswersForm, StyledAnswer, Card } from "./styles";
export default class Answers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      guess: false
    };
  }

  handleGuess = e => {
    e.preventDefault();
    const { handleRound, getNextQuestion, currentPlayer } = this.props;
    const { guess } = this.state;

    if (!guess) {
      return;
    } else if (!guess.correct) {
      handleRound({ correct: false, player: currentPlayer });
    } else {
      handleRound({ correct: guess.correct, player: currentPlayer });
    }
    getNextQuestion();
  };
  handleClick = guess => {
    this.setState({ guess: guess });
  };

  render() {
    const { answers } = this.props;
    const { correct_answer, incorrect_answers } = answers;

    const { guess } = this.state;
    let modAnswers = [{ correct: true, question: correct_answer }];

    incorrect_answers.map(
      (a, i) => (modAnswers[i + 1] = { correct: false, question: a })
    );
    return (
      <>
        <AnswersForm onSubmit={e => this.handleGuess(e)}>
          <Card>
            {modAnswers.map((q, i) => (
              <StyledAnswer key={i}>
                <label data-testid="question">
                  {decodeURIComponent(q.question)}
                </label>
                <input
                  name={`answer${i}`}
                  type="checkbox"
                  value={q.question}
                  checked={guess.question === q.question}
                  onChange={() => this.handleClick(q)}
                />
              </StyledAnswer>
            ))}
          </Card>
          <input type="submit" value="Guess" />
        </AnswersForm>
      </>
    );
  }
}
