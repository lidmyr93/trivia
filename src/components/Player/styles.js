import styled from "styled-components";

export const PlayerContainer = styled.div`
  display: flex;
  flex-direction: column;
  background-color: transparent;
  min-width: 50%;
  height: 100%;
  align-items: center;
  border-bottom: 1px solid black;
  margin: 2px 0;
  .direction-row {
    display: flex;
    width: 50%;
    justify-content: space-evenly;
    align-items: center;
    padding: 5px;
  }
  h3 {
    font-size: 1.5em;
    margin: 0;
  }
  button {
    background-color: transparent;
    border: 2px solid red;
    border-radius: 0.6em;
    color: red;
    cursor: pointer;
    display: flex;
    align-self: center;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1;
    margin: 5px;
    padding: 0.3em 0.7em;
    text-decoration: none;
    text-align: center;
    text-transform: uppercase;
    font-family: "Montserrat", sans-serif;
    font-weight: 700;

    &:hover,
    &:focus {
      color: #fff;
      outline: 0;
    }

    border-color: red;
    color: white;
    box-shadow: 0 0 40px 40px red inset, 0 0 0 0 red;
    transition: all 150ms ease-in-out;

    &:hover {
      box-shadow: 0 0 10px 0 red inset, 0 0 10px 4px red;
      color: red;
    }
  }
`;
