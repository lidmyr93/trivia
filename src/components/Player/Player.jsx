import React from "react";
import PlayerPointTable from "../PlayerPointTable/PlayerPointTable";
import { PlayerContainer } from "./styles";
export default function Player(props) {
  const { playerName, gameState, handleRemove, playerScore } = props;
  return (
    <PlayerContainer className="row">
      <div className="direction-row">
        <h3 className="playerName">{playerName}</h3>
        {!gameState && (
          <button value={playerName} onClick={e => handleRemove(e)}>
            X
          </button>
        )}
      </div>
      {gameState && (
        <PlayerPointTable playerScore={playerScore} player={playerName} />
      )}
    </PlayerContainer>
  );
}
