import React from "react";
import { mount, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { toHaveTextContent, toHaveValue } from "@testing-library/jest-dom";
import { render } from "@testing-library/react";

import Answers from "../components/Answers/Answers";

const mockData = {
  category: "Entertainment: Video Games",
  type: "multiple",
  difficulty: "easy",
  question: "Who created the digital distribution platform Steam?",
  correct_answer: "Valve",
  incorrect_answers: ["Pixeltail Games", "Ubisoft", "Electronic Arts"]
};
configure({ adapter: new Adapter() });
expect.extend({ toHaveTextContent, toHaveValue });

describe("Answers functions", () => {
  test("handleClick to set state correctly", () => {
    const wrapper = mount(<Answers answers={mockData} />);
    expect(wrapper.state().guess).toBeFalsy();
    expect(wrapper.props().answers).toBeTruthy();

    wrapper.instance().handleClick(mockData.correct_answer);
    expect(wrapper.state().guess).toEqual("Valve");
  });

  test("handleGuess to work as intented when guess is correct", () => {
    const mockFn = jest.fn(() => {});
    const wrapper = mount(
      <Answers
        handleRound={mockFn}
        getNextQuestion={mockFn}
        answers={mockData}
        currentPlayer="playerOne"
      />
    );
    wrapper.setState({ guess: { correct: true, question: "Valve" } });

    let btn = wrapper.find('input[type="submit"]');
    btn.simulate("submit", {
      preventDefault() {}
    });
    expect(mockFn).toHaveBeenCalledTimes(2);
    expect(mockFn).toHaveBeenCalledWith({ correct: true, player: "playerOne" });
  });
  test("handleGuess to work as intented when guess is not correct", () => {
    const mockFn = jest.fn(() => {});
    const wrapper = mount(
      <Answers
        handleRound={mockFn}
        getNextQuestion={mockFn}
        answers={mockData}
        currentPlayer="playerOne"
      />
    );
    wrapper.setState({ guess: { correct: false, question: "Valve" } });

    let btn = wrapper.find('input[type="submit"]');
    btn.simulate("submit", {
      preventDefault() {}
    });
    expect(mockFn).toHaveBeenCalledTimes(2);
    expect(mockFn).toHaveBeenCalledWith({
      correct: false,
      player: "playerOne"
    });
  });
  test("handleGuess to return if guess is not set", () => {
    const mockFn = jest.fn(() => {});
    const wrapper = mount(
      <Answers
        handleRound={mockFn}
        getNextQuestion={mockFn}
        answers={mockData}
        currentPlayer="playerOne"
      />
    );
    wrapper.setState({ guess: false });

    let btn = wrapper.find('input[type="submit"]');
    btn.simulate("submit", {
      preventDefault() {}
    });
    expect(mockFn).toHaveBeenCalledTimes(0);
  });
});

test("Component rendering 4 answers", () => {
  const { queryAllByTestId } = render(<Answers answers={mockData} />);
  const answer = queryAllByTestId("question");

  expect(answer).toHaveLength(4);
  expect(answer[0]).toHaveTextContent("Valve");
  expect(answer[1]).toHaveTextContent("Pixeltail Games");
  expect(answer[2]).toHaveTextContent("Ubisoft");
  expect(answer[3]).toHaveTextContent("Electronic Arts");
});
