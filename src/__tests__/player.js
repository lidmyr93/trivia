import React from "react";
import { mount, shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { toHaveTextContent, toHaveValue } from "@testing-library/jest-dom";
import { render, fireEvent } from "@testing-library/react";
import Player from "../components/Player/Player";

import PlayerPointTable from "../components/PlayerPointTable/PlayerPointTable";

configure({ adapter: new Adapter() });
expect.extend({ toHaveTextContent, toHaveValue });
test("Player compontents props and functions when game has started", () => {
  const wrapper = mount(
    <Player
      playerName="playerOne"
      gameState={true}
      playerScore={[true]}
      handleRemove={() => {}}
    />
  );

  expect(wrapper.props().playerName).toContain("playerOne");
  expect(wrapper.props().gameState).toBeTruthy();
  expect(wrapper.props().playerScore).toBeTruthy();

  expect(() => {
    wrapper.find("button").simulate("click");
  }).toThrow();
  expect(wrapper.find(<PlayerPointTable />)).toBeTruthy();
});

test("Compontent props and functions when game has not started", () => {
  const mockFn = jest.fn();
  const wrapper = mount(
    <Player
      playerName="playerOne"
      gameState={false}
      playerScore={[]}
      handleRemove={mockFn}
    />
  );
  let btn = wrapper.find("button");
  expect(wrapper.props().playerName).toContain("playerOne");
  expect(wrapper.props().gameState).toBeFalsy();
  expect(wrapper.props().playerScore).toEqual([]);
  expect(btn).toBeTruthy();

  btn.simulate("click");
  expect(mockFn).toHaveBeenCalledTimes(1);
});

test("Component rendering", () => {
  const { getByText } = render(
    <Player playerName="playerOne" gameState={false} playerScore={[]} />
  );
  const player = getByText("playerOne");
  const btn = getByText("X");
  expect(player).toHaveTextContent("playerOne");

  //Important for handeRemove to work
  expect(btn).toHaveValue("playerOne");
});
