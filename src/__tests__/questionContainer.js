import React from "react";
import { mount, shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import QuestionContainer from "../components/questionContainer/QuestionContainer";
beforeEach(() => {
  mockData = [
    {
      category: "Test",
      type: "test",
      difficulty: "test",
      question: "test",
      correct_answer: "test"
    },
    {
      category: "Test2",
      type: "test",
      difficulty: "test",
      question: "test",
      correct_answer: "test"
    },
    {
      category: "Test3",
      type: "test",
      difficulty: "test",
      question: "test",
      correct_answer: "test"
    }
  ];
});

configure({ adapter: new Adapter() });
let mockData;

const playerOne = {
  playerName: "playerOne",
  score: [],
  index: 0
};
const playerTwo = {
  playerName: "playerTwo",
  score: [],
  index: 1
};
const playerThree = {
  playerName: "playerThree",
  score: [],
  index: 2
};
describe("QuestionContainer onMount", () => {
  test("Should get data on mount", () => {
    const wrapper = shallow(<QuestionContainer data={mockData} />);
    expect(wrapper.state().data).toBeTruthy();
  });
});

test("handleNextQuestion", () => {
  const wrapper = shallow(<QuestionContainer data={mockData} />);
  expect(wrapper.state().selectedQuestion).toBeNull();
  wrapper.instance().handleNextQuestion();
  expect(wrapper.state().selectedQuestion).toBeTruthy();
});

describe("Player functions", () => {
  test("handle add players should add player to state", () => {
    const wrapper = shallow(<QuestionContainer />);
    //Single
    wrapper.instance().handleAddPlayers("playerOne");
    expect(wrapper.state().players).toContain("playerOne");

    //Multipel players
    wrapper.instance().handleAddPlayers("playerTwo");
    wrapper.instance().handleAddPlayers("playerThree");
    expect(wrapper.state().players).toContain(
      "playerOne",
      "playerTwo",
      "playerThree"
    );
  });
});

test("handleRemovePlayers should remove player from state", () => {
  const wrapper = shallow(<QuestionContainer />);
  wrapper.setState({ players: [playerOne, playerTwo, playerThree] });
  expect(wrapper.state().players).toHaveLength(3);
  wrapper.instance().handleRemovePlayers("playerOne");
  expect(wrapper.state().players).toHaveLength(2);
});

test("handleNextPlayer should set next player to state", () => {
  const wrapper = shallow(<QuestionContainer />);
  wrapper.instance().handleAddPlayers("playerOne");
  wrapper.instance().handleAddPlayers("playerTwo");
  wrapper.instance().handleAddPlayers("playerThree");
  expect(wrapper.state().currentPlayer).toEqual(null);
  wrapper.instance().handleNextPlayer();
  expect(wrapper.state().currentPlayer).toBeTruthy();
  expect(wrapper.state().currentPlayer).toContain("playerOne", "playerTwo");
});

test("handleGamestart should set correct state and call handleNextplayer", () => {
  let mockFn = jest.fn();
  const wrapper = shallow(<QuestionContainer />);
  wrapper.instance().handleNextPlayer = mockFn;
  wrapper.update();
  expect(wrapper.state().gameState).not.toBeTruthy();
  wrapper.instance().handleAddPlayers("playerOne");
  wrapper.instance().handleAddPlayers("playerTwo");
  wrapper.instance().handleGameStart();

  expect(wrapper.state().gameState).toBeTruthy();
  expect(mockFn).toHaveBeenCalledTimes(1);
});

describe("handleRound()", () => {
  let mockFnOne = jest.fn();
  let mockFnTwo = jest.fn();
  const wrapper = shallow(<QuestionContainer />);
  wrapper.instance().handleNextPlayer = mockFnOne;
  wrapper.instance().handlePoints = mockFnTwo;
  wrapper.update();
  test("handleRound should call handleNextPlayer", () => {
    wrapper.instance().handleRound();
    expect(mockFnOne).toHaveBeenCalledTimes(1);
  });

  test("handleRound should call handlePoints with roundInfo", () => {
    wrapper.instance().handleRound("test");
    expect(mockFnTwo).toHaveBeenCalledTimes(2);
    expect(mockFnTwo).toHaveBeenCalledWith("test");
  });
});

describe("handlePoints", () => {
  const roundInfo = { correct: true };
  test("Should set points correctly", () => {
    const wrapper = shallow(<QuestionContainer />);
    const addplayer = wrapper.instance().handleAddPlayers;
    addplayer(playerOne);
    addplayer(playerTwo);
    addplayer(playerThree);
    expect(wrapper.state().players).toHaveLength(3);

    wrapper.instance().handleNextPlayer();
    wrapper.instance().handlePoints(roundInfo);
    expect(wrapper.state().currentPlayer).toBeTruthy();
    expect(wrapper.state().currentPlayer.score).toHaveLength(1);
  });

  test("Should call handlewin if player get max points", () => {
    const mockFn = jest.fn();
    const wrapper = shallow(<QuestionContainer />);
    wrapper.instance().handleWin = mockFn;
    wrapper.update();
    const addplayer = wrapper.instance().handleAddPlayers;
    playerOne.score = [true, true, true, true];
    addplayer(playerOne);
    addplayer(playerTwo);
    addplayer(playerThree);
    expect(wrapper.state().players).toHaveLength(3);

    wrapper.instance().handleNextPlayer();
    wrapper.instance().handlePoints(roundInfo);
    expect(mockFn).toHaveBeenCalledTimes(1);
  });
});

test("handleWin should set state correctly", () => {
  const wrapper = shallow(<QuestionContainer />);
  expect(wrapper.state().winningPlayer).not.toBeTruthy();

  wrapper.instance().handleWin(playerOne);

  expect(wrapper.state().winningPlayer).toBeTruthy();
  expect(wrapper.state().winningPlayer).toEqual(playerOne);
});

test("handleReset should reset correct state to restart match", () => {
  const wrapper = shallow(<QuestionContainer />);
  const addplayer = wrapper.instance().handleAddPlayers;
  addplayer(playerOne);
  addplayer(playerTwo);
  wrapper.setState({
    gameState: true,
    winningPlayer: playerOne
  });
  wrapper.state().players[0].score = [true, true, true, true];
  wrapper.state().players[1].score = [true, true, true, true];

  wrapper.instance().handleReset();
  expect(wrapper.state().gameState).toBeFalsy();
  expect(wrapper.state().winningPlayer).toBeNull();
  expect(wrapper.state().players[0].score).toEqual([]);
  expect(wrapper.state().players[1].score).toEqual([]);
});
