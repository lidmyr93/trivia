import React from "react";
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { toHaveTextContent, toHaveValue } from "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import QuestionHeader from "../components/QuestionHeader/QuestionHeader";

configure({ adapter: new Adapter() });
expect.extend({ toHaveTextContent, toHaveValue });

test("Questionheader should return right text", () => {
  const { getByTestId } = render(
    <QuestionHeader category="category" question="question" />
  );

  expect(getByTestId("category")).toHaveTextContent("category");
  expect(getByTestId("question")).toHaveTextContent("question");
});
