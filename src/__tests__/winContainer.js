import React from "react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { toHaveTextContent, toHaveValue } from "@testing-library/jest-dom";
import { render } from "@testing-library/react";

import WinContainer from "../components/winContainer/WinContainer";

configure({ adapter: new Adapter() });
expect.extend({ toHaveTextContent, toHaveValue });

describe("Wincontainer", () => {
  test("Onclick function gets called", () => {
    const mockFn = jest.fn();
    const wrapper = shallow(
      <WinContainer
        winningPlayer={{ playerName: "playerOne" }}
        handleReset={mockFn}
      />
    );
    let btn = wrapper.find("button");
    btn.simulate("click");

    expect(mockFn).toHaveBeenCalledTimes(1);
  });

  test("Should render the right name", () => {
    const { getByTestId } = render(
      <WinContainer winningPlayer={{ playerName: "playerOne" }} />
    );
    const player = getByTestId("winningPlayer");

    expect(player).toHaveTextContent("playerOne");
  });
});
