import React from "react";
import { mount, shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import PlayerTable from "../components/playerTable/PlayerTable";

configure({ adapter: new Adapter() });
const playerOne = {
  playerName: "playerOne",
  score: [],
  index: 0
};
const playerTwo = {
  playerName: "playerTwo",
  score: [],
  index: 1
};
const playerThree = {
  playerName: "playerThree",
  score: [],
  index: 2
};

//handlesubmit
describe("form", () => {
  test("handleSubmit gets called when button is pressed", () => {
    const mockFn = jest.fn();
    const wrapper = mount(<PlayerTable handleAddPlayers={mockFn} />);
    expect(wrapper.state().index).toEqual(0);
    expect(mockFn).toHaveBeenCalledTimes(0);
    let fn = wrapper.find('input[name="addPlayer"]');
    fn.simulate("submit");
    //handleSubmit functions calls handleAddplayers and setState
    //Checked if that function have been called when submited
    //And that the setState operation has worked
    //Had error trying to check handleSubmit function
    expect(mockFn).toHaveBeenCalledTimes(1);
    expect(wrapper.state().index).toEqual(1);
  });

  test("handleChange gets called on change", () => {
    const wrapper = shallow(<PlayerTable />);
    let fn = wrapper.find('input[name="playerName"]');

    fn.simulate("change", { target: { value: "test" } });
    expect(wrapper.state().addedPlayer.playerName).toBe("test");
  });
});

//handleStartlick
describe("handleStartClick", () => {
  test("handleStartClick gets called when button is clicked", () => {
    const mockFn = jest.fn();
    const wrapper = shallow(<PlayerTable handleGameStart={mockFn} />);

    wrapper.setState({ addedPlayer: playerOne });
    wrapper.setState({ index: 1 });
    wrapper.update();
    wrapper.find(".button").simulate("click");
    expect(wrapper.state().addedPlayer).toBeNull();
    expect(wrapper.state().index).toEqual(0);
    expect(mockFn).toHaveBeenCalledTimes(1);
  });
});
