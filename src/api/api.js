const API_URL = `https://opentdb.com/api.php?amount=50&category=15&difficulty=easy&type=multiple&encode=url3986`;

export const getQuestions = query => {
  const url = `${API_URL}`;
  return fetch(url)
    .then(response => response.json())
    .then(json => json.results);
};
